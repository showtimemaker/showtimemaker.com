export default function Error({ statusCode }) {
  return (
    <div className="content">
      <h1>{statusCode
        ? `错误： ${statusCode} `
        : '未知错误'}</h1>
    </div>
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}
