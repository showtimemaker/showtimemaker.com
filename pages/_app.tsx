import Head from 'next/head'
import '../style/index.css'

export default ({ Component, pageProps })=>{
  return (
    <>
      <Head>
        <meta charSet="UTF-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" sizes='32x32'/> 
        <title>ShowTimeMaker ｜ 优秀工具制造商</title>
      </Head>
      <Component {...pageProps}/>
    </>
  )
}
